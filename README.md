# cljs-workshop

ClojureScript playground project

## Setup

To get an interactive development environment running execute the following:

    export GITHUB_TOKEN=someToken && lein figwheel

and open your browser at [localhost:3449](http://localhost:3449/).
This will auto compile and send all changes to the browser without the
need to reload. After the compilation process is complete, you will
get a Browser Connected REPL. An easy way to try it is:

    (js/alert "Am I connected?")

and you should see an alert in the browser window.

To clean all compiled files:

    lein clean

To create a production build run:

    lein cljsbuild once min

And open your browser in `resources/public/index.html`. You will not
get live reloading, nor a REPL. 

### Figwheel Notes

*^:figwheel-always* - meta data for namespace, to reload file every time file changes

## Links
- [ClojureScript -> JavaScript Interop](http://www.spacjer.com/blog/2014/09/12/clojurescript-javascript-interop/)
- [Awsome Frameworks, Libraries and etc](https://github.com/emrehan/awesome-clojurescript)
- [ClojureScript Workshop](https://www.niwi.nz/cljs-workshop/#_introduction)

## License

Copyright © 2016 by someone who has no clue about copyrights
