(defproject cljs-workshop "0.1.0-SNAPSHOT"
  :description "Demo CLJS coolness ;)"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :min-lein-version "2.5.3"
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/clojurescript "1.7.228"]
                 [org.clojure/core.async "0.2.374"]
                 [adzerk/env "0.3.0"]
                 [cljs-http "0.1.39"]
                 [hipo "0.5.2"]]

  :plugins [[lein-cljsbuild "1.1.3"]
            [lein-figwheel "0.5.0-6"]]
  
  :source-paths ["src"]

  :clean-targets ^{:protect false} ["resources/public/js/compiled" "target"]

  :cljsbuild {:builds [{:id "dev"
                        :source-paths ["src"]
                        :figwheel true
                        :compiler {:main cljs-workshop.core
                                   :asset-path "js/compiled/out"
                                   :output-to "resources/public/js/compiled/cljs_workshop.js"
                                   :output-dir "resources/public/js/compiled/out"
                                   :source-map-timestamp true }}
                       {:id "min"
                        :source-paths ["src"]
                        :compiler {:output-to "resources/public/js/compiled/cljs_workshop.js"
                                   :main cljs-workshop.core
                                   :optimizations :advanced
                                   :pretty-print false}}]}
  
  :profiles {:dev {:dependencies [[com.cemerick/piggieback "0.2.1"]
                                  [figwheel-sidecar "0.5.0-1"]]
                   :repl-options {:nrepl-middleware [cemerick.piggieback/wrap-cljs-repl]}}}
  :figwheel {:css-dirs ["resources/public/css"]
             :nrepl-port 7888
             :repl false})
