(ns ^:figwheel-always cljs-workshop.core
  (:require-macros [cljs.core.async.macros :refer [go go-loop]]
                   [adzerk.env :as env])
  (:require [cljs.core.async :refer [put! chan <!]]
            [goog.dom :as dom]
            [goog.events :as events]
            [cljs-http.client :as http]
            [cljs-workshop.ui :as ui]))

(enable-console-print!)

(env/def GITHUB_TOKEN :required)
(def api-url "https://api.github.com/search/repositories")
